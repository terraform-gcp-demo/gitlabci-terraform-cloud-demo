#!/bin/bash

# **Important** This script requires curl and jq utilities
# **Important** Both TFC_ORG and TFC_TOKEN environment variables should be set prior to running this script
# This script uploads a Sentinel policy to Terraform Cloud using the Policies API
# We assume the workspace name is my-gcp-gitlab-pipeline, please adjust this if needed
# We assume the TFC hostname name is app.terraform.io, please adjust this if needed
export TFC_ADDR="app.terraform.io"
if [ -z $TFC_WORKSPACE ]; then
  # Override default workspace name if needed
  export TFC_WORKSPACE="my-gcp-gitlab-pipeline"
  echo "Using default Workspace name: $TFC_WORKSPACE"
fi

# Validate that variables are set and obtain the workspace ID
./check_tfc_vars.sh
workspace_id=$(cat workspace_id)
# Check exit code
if [ -z "$workspace_id" ]
then
  echo "Could not get workspace ID, exitting."
  exit 
fi

# Adjust policy set name if needed. If so, please also adjust it in the delete.sh script.
policy_set_name="gitlabci-gcp-policies"

# Check for previous policy set and delete it if it exists
policy_set_id=$(curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" \
      "https://${TFC_ADDR}/api/v2/organizations/${TFC_ORG}/policy-sets?search%5Bname%5D=${policy_set_name}" | jq -r '.data[0].id')
if [ ! -z "$policy_set_id" ] && [ "$policy_set_id" != "null" ];
then
  echo "Found previous policy set with name: ${policy_set_name} and id: ${policy_set_id}"
  echo "Deleting previous policy set."
  curl --header "Authorization: Bearer ${TFC_TOKEN}" \
  --request DELETE "https://${TFC_ADDR}/api/v2/policy-sets/${policy_set_id}"
else
  echo "Did not find an existing policy set with name: ${policy_set_name}"
fi

# Check if GOOGLE_CREDENTIALS_PATH variable is set
# If so delete existing GOOGLE_CREDENTIALS variable and set the new one
if [ ! -z "$GOOGLE_CREDENTIALS_PATH" ]; then
  echo "Setting new GOOGLE_CREDENTIALS from $GOOGLE_CREDENTIALS_PATH"
  export GOOGLE_CREDENTIALS=$(tr '\n' ' ' < $GOOGLE_CREDENTIALS_PATH | sed -e 's/\"/\\\\"/g' -e 's/\//\\\//g' -e 's/\\n/\\\\\\\\n/g')

  # query existing vars:
  curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" "https://${TFC_ADDR}/api/v2/vars?filter%5Borganization%5D%5Bname%5D=${TFC_ORG}&filter%5Bworkspace%5D%5Bname%5D=${TFC_WORKSPACE}" | jq -r . > vars.json
  var_id=$(cat vars.json | grep -C 5 GOOGLE_CREDENTIALS | grep -i id | awk -F '"' '{print $4}')
  if [ ! -z $var_id ]; then
    echo "Found previously set credentials. Overwriting."
    curl \
    --header "Authorization: Bearer $TFC_TOKEN" \
    --header "Content-Type: application/vnd.api+json" \
    --request DELETE \
    https://${TFC_ADDR}/api/v2/vars/${var_id}

    sed -e "s/my-key/GOOGLE_CREDENTIALS/" -e "s/my-hcl/false/" -e "s/my-value/${GOOGLE_CREDENTIALS}/" -e "s/my-category/env/" -e "s/my-sensitive/true/" -e "s/my-workspace-id/${workspace_id}/" < ../api_templates/variable.json.template  > variable.json;
    curl --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" --data @variable.json "https://${TFC_ADDR}/api/v2/vars"
  fi
  rm -f vars.json variable.json
fi

# Write payload
cat <<EOF >payload.json
{
  "data": {
    "attributes": {
      "is-destroy":true,
      "message": "Destroy workspace using API"
    },
    "type":"runs",
    "relationships": {
      "workspace": {
        "data": {
          "type": "workspaces", "id": "$workspace_id"
        }
      }
    }
  }
}
EOF

echo "Queueing a destroy Run"
curl --header "Authorization: Bearer ${TFC_TOKEN}" \
  --header "Content-Type: application/vnd.api+json" \
  --request POST \
  --data @payload.json \
  https://${TFC_ADDR}/api/v2/runs > destroy_run.json

run_id=$(cat destroy_run.json | jq -r .data.id)
echo "Queued Response Run ID: ${run_id}"

# Evaluate $run_id and display URL
if [ ! -z "$run_id" ]; then
  url="https://${TFC_ADDR}/app/${TFC_ORG}/workspaces/${TFC_WORKSPACE}/runs/${run_id}"
  echo "A Destroy Run was queued successfully. Pausing 120 seconds for Plan to complete before applying Destroy Run"
  echo "Please visit the following URL to see the Run"
  echo "$url"
  sleep 120
  echo "Applying Run"
  curl \
  --header "Authorization: Bearer $TFC_TOKEN" \
  --header "Content-Type: application/vnd.api+json" \
  --request POST https://${TFC_ADDR}/api/v2/runs/$run_id/actions/apply

  echo "**Important**: Please visit the following URL to ensure destroy completed successfully."
  echo "$url"
else
  echo "Did not get a successful Run ID. Please retry or queue a Destroy Run from the TFC UI below"
  echo "https://${TFC_ADDR}/app/${TFC_ORG}/workspaces/${TFC_WORKSPACE}/runs"
fi

